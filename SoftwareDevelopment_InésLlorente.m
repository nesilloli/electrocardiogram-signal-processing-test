%Loading the signal
load('ecgConditioningExample.mat');

%Time vector and visualization
time = 1/fs:1/fs:30000/fs;

figure;
plot(time, ecg); xlim([8 11]); ylim([0 16e4]);
legend('Channel 1','Channel 2','Channel 3','Channel 4','Channel 5','Channel 6');
title('ECG sginal'); xlabel('Time (sg)'); ylabel('Voltage (mV)');

%Detect disconnected channels - vector connected_chanel is 1 when the
%channel is disconnected
for k = 1:size(ecg,2)
    
    channel = find(ecg(:,k) == 0);
    
    if isempty(channel) == 1 
        connected_chanel(k) = 0;
    else
        mensaje = ['Channel ', num2str(k), ' is not connected'];
        disp(mensaje);
        connected_chanel(k) = 1; 
    end
    
end

%Recording spikes removal
ecg = ecg(:,connected_chanel == 0); %only connected channels
for j = 1:size(ecg,2)
    
    artefacts = find(abs(ecg(:,j)) > 10*(median(ecg(:,j))));
    
    if isempty(artefacts) == 0
        for m = 1:length(artefacts)
            ecg(artefacts(m),j) = min(ecg(artefacts(m)-1,j),ecg(artefacts(m)+1,j));
        end
    end
    
end

Fn = fs/2; %Nyquist frequency

%Low-pass filtering
Wn_low = 45/Fn; %Normalized cutt-off frequencies
n=1; %Filter order
[a,b] = butter(n, Wn_low, 'low');
ecg_filtered = filtfilt(a, b, ecg);

figure;
plot(time, ecg_filtered); xlim([8 11]); ylim([0 16e4]);
title('ECG low-pass filtering'); xlabel('Time (sg)'); ylabel('Voltage (mV)');

%Powerline interference filtering
wo = 50/Fn;  
bw = wo/35;
[c,d] = iirnotch(wo,bw);
%fvtool(c,d)
ecg_filtered2 = filter(c, d, ecg_filtered);

figure;
plot(time, ecg_filtered2); xlim([8 11]); ylim([0 16e4]);
title('ECG powerline interference filtering'); xlabel('Time (sg)'); ylabel('Voltage (mV)');
